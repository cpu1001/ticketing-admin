# Docker Vue 

> Docker-compose for @vue/cli project

Docker and docker-compose are required to run this project, please make sure they are installed.


# Running app using Docker: 

Build container:

```sh
$ docker-compose build
```

Create and run project - in the root folder of the project run this command:

```sh
$ docker-compose up
or
$ docker-compose run --rm --service-ports vue
```

Access project via browser:

```sh
http://localhost:5173/
```

Login credentials:

```sh
test@test.com | test
```


# Running app locally and running unit tests: 

Please use nvm to switch local version of node to v20

 ```sh
$ nvm add 20.11.1
$ nvm use 20
```

Navigate to /web folder, install dependencies and start the app

 ```sh
$ cd web/
$ npm install
$ npm run dev

```

> *!!* If you get `Error: EACCES: permission denied...` error when starting the app please run this comand before trying again:

 ```sh
 $ sudo rm -rf node_modules/.vite
```

Running unit tests, inside web/ folder run this command: 

 ```sh
 $ npm run test:unit
```



