# services/vue/Dockerfile
FROM node:16

WORKDIR /www

COPY package*.json ./
RUN npm install

COPY . .

EXPOSE 5173
CMD ["npm", "run", "dev"]