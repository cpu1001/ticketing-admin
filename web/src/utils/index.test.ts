import { describe, it, expect, beforeEach, vi } from 'vitest'
import { isAuthenticated, formatDate, formatDateForDatePicker } from './index'

describe('isAuthenticated', () => {
  beforeEach(() => {
    // Clear the localStorage before each test
    localStorage.clear()
  })

  it('should return true if email is in localStorage', () => {
    localStorage.setItem('email', 'test@example.com')
    expect(isAuthenticated()).toBe(true)
  })

  it('should return false if email is not in localStorage', () => {
    expect(isAuthenticated()).toBe(false)
  })
})

describe('formatDate', () => {
  it('should format date correctly', () => {
    const input = '2024-05-15T00:00:00.000Z'
    const output = '16/5/2024'
    expect(formatDate(input)).toBe(output)
  })

  it('should handle invalid date input gracefully', () => {
    const input = 'invalid-date'
    expect(() => formatDate(input)).toThrow()
  })
})

describe('formatDateForDatePicker', () => {
  it('should format date correctly for date picker', () => {
    const input = '2024-05-15T00:00:00.000Z'
    const output = '2024-05-15'
    expect(formatDateForDatePicker(input)).toBe(output)
  })

  it('should handle invalid date input gracefully', () => {
    const input = 'invalid-date'
    expect(() => formatDateForDatePicker(input)).toThrow()
  })
})