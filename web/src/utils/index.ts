export function isAuthenticated(): boolean {
  return localStorage.getItem('email') ? true : false
}

// get EU format: 16.5.2024
export const formatDate = (dateString: string): string => {
  const dateObject = new Date(dateString)

  if (isNaN(dateObject.getTime())) {
    throw new Error('Invalid date')
  }

  const year = dateObject.getUTCFullYear()
  const month = dateObject.getUTCMonth() + 1
  const day = dateObject.getUTCDate() + 1

  const formattedDate = day + '/' + month + '/' + year

  return formattedDate
}

export const formatDateForDatePicker = (dateString: string): string => {
  const dateObject = new Date(dateString)

  if (isNaN(dateObject.getTime())) {
    throw new Error('Invalid date')
  }

  const year = dateObject.getUTCFullYear()
  const month = String(dateObject.getUTCMonth() + 1).padStart(2, '0')
  const day = String(dateObject.getUTCDate()).padStart(2, '0')

  const formattedDate = `${year}-${month}-${day}`

  return formattedDate
}
