import '@/assets/main.css'
import '@mdi/font/css/materialdesignicons.css'
import { createApp } from 'vue'
import { store } from './store'

// Vuetify
import 'vuetify/styles'
import { createVuetify } from 'vuetify'
import { VDateInput } from 'vuetify/labs/VDateInput'
import * as components from 'vuetify/components'
import * as directives from 'vuetify/directives'

import App from './App.vue'
import router from './router'

// Mirage
import { makeServer } from './server'

if (process.env.NODE_ENV === 'development') {
  makeServer()
}

const vuetify = createVuetify({
  components: {
    ...components,
    VDateInput
  },
  directives
})

const app = createApp(App)

app.use(router)
app.use(store)
app.use(vuetify)
app.mount('#app')
