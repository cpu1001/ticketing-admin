import axios from 'axios'
import LocalStorageService from './local-storage.service'

export default () => {
  const config = {
    baseURL: 'http://localhost:8081/api/'
  }

  const instance = axios.create(config)
  const token = LocalStorageService.getToken()

  if (token) {
    instance.defaults.headers.common['Authorization'] = token
  }

  return instance
}
