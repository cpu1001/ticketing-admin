import Api from './api'
import type { LoginPayloadType } from '@/types/auth.types'

export default {
  login(params: LoginPayloadType) {
    return Api().post('auth/login', params)
  }
}
