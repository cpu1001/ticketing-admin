export default {
  getToken() {
    const token = window.localStorage.getItem('token')
    return token
  }
}
