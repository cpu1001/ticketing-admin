import type { EventType } from '@/types/events.types'
import Api from './api'

export default {
  getEvents() {
    return Api().get('events')
  },

  getEvent(id: string) {
    return Api().get(`events/${id}`)
  },

  updateEvent(data: EventType) {
    return Api().patch(`events/${data.id}`, data)
  },

  createEvent(data: EventType) {
    return Api().post(`events`, data)
  },

  deleteEvent(data: EventType) {
    return Api().delete(`events/${data.id}`)
  }
}
