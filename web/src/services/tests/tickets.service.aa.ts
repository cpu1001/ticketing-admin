import { describe, it, expect, vi, beforeEach } from 'vitest';
import type { TicketType } from '@/types/tickets.types';
import Api from '../api';
import ticketService from '../tickets.service'; 

vi.mock('./api', () => ({
  default: () => ({
    get: vi.fn(),
    post: vi.fn(),
    patch: vi.fn(),
    delete: vi.fn()
  })
}));

describe('ticketService', () => {
  let apiMock: any;

  beforeEach(() => {
    apiMock = Api();
  });

  describe('getTickets', () => {
    it('should call Api.get with correct URL', async () => {
      apiMock.get.mockResolvedValue({ data: 'response' });
      
      const response = await ticketService.getTickets();

      expect(apiMock.get).toHaveBeenCalledWith('tickets');
      expect(response).toEqual({ data: 'response' });
    });
  });

  describe('getTicket', () => {
    it('should call Api.get with correct URL and id', async () => {
      const id = '1';
      apiMock.get.mockResolvedValue({ data: 'response' });
      
      const response = await ticketService.getTicket(id);

      expect(apiMock.get).toHaveBeenCalledWith(`tickets/${id}`);
      expect(response).toEqual({ data: 'response' });
    });
  });

  describe('updateTicket', () => {
    it('should call Api.patch with correct URL and data', async () => {
      const data: TicketType = { id: '1', title: 'Updated Title', description: 'Updated Description' };
      apiMock.patch.mockResolvedValue({ data: 'response' });
      
      const response = await ticketService.updateTicket(data);

      expect(apiMock.patch).toHaveBeenCalledWith(`tickets/${data.id}`, data);
      expect(response).toEqual({ data: 'response' });
    });
  });

  describe('createTicket', () => {
    it('should call Api.post with correct URL and data', async () => {
      const data: TicketType = { id: '1', title: 'New Ticket', description: 'New Description' };
      apiMock.post.mockResolvedValue({ data: 'response' });
      
      const response = await ticketService.createTicket(data);

      expect(apiMock.post).toHaveBeenCalledWith('tickets', data);
      expect(response).toEqual({ data: 'response' });
    });
  });

  describe('deleteTicket', () => {
    it('should call Api.delete with correct URL and data', async () => {
      const data: TicketType = { id: '1', title: 'Delete Ticket', description: 'Description' };
      apiMock.delete.mockResolvedValue({ data: 'response' });
      
      const response = await ticketService.deleteTicket(data);

      expect(apiMock.delete).toHaveBeenCalledWith(`tickets/${data.id}`);
      expect(response).toEqual({ data: 'response' });
    });
  });
});