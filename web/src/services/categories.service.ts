import type { CategoryType } from '@/types/categories.types'
import Api from './api'

export default {
  getCategories() {
    return Api().get('categories')
  },

  getCategory(id: string) {
    return Api().get(`categories/${id}`)
  },

  updateCategory(data: CategoryType) {
    return Api().patch(`categories/${data.id}`, data)
  },

  createCategory(data: CategoryType) {
    return Api().post(`categories`, data)
  },

  deleteCategory(data: CategoryType) {
    return Api().delete(`categories/${data.id}`)
  }
}
