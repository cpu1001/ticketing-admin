import type { TicketType } from '@/types/tickets.types'
import Api from './api'

export default {
  getTickets() {
    return Api().get('tickets')
  },

  getTicket(id: string) {
    return Api().get(`tickets/${id}`)
  },

  updateTicket(data: TicketType) {
    return Api().patch(`tickets/${data.id}`, data)
  },

  createTicket(data: TicketType) {
    return Api().post(`tickets`, data)
  },

  deleteTicket(data: TicketType) {
    return Api().delete(`tickets/${data.id}`)
  }
}
