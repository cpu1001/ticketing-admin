import { createServer, Model, Response } from 'miragejs'

export function makeServer({ environment = 'development' } = {}) {
  const tickets = [
    {
      id: 1,
      name: 'Metallica Tour - General Admission',
      price: 80,
      event: {
        id: 1,
        name: 'Metallica Concert'
      },
      category: {
        id: 1,
        name: 'General Admission'
      },
      totalAvailability: 100,
      available: 100
    },
    {
      id: 2,
      name: 'Metallica Tour - VIP Pass',
      price: 200,
      event: {
        id: 1,
        name: 'Metallica Concert'
      },
      category: {
        id: 2,
        name: 'VIP Pass'
      },
      totalAvailability: 100,
      available: 100
    },
    {
      id: 3,
      name: 'Metallica Tour - Front Row Seat',
      price: 100,
      event: {
        id: 1,
        name: 'Metallica Concert'
      },
      category: {
        id: 3,
        name: 'Front Row Seat'
      },
      totalAvailability: 100,
      available: 100
    },
    {
      id: 4,
      name: 'Metallica Tour - Regular Seat',
      price: 50,
      event: {
        id: 1,
        name: 'Metallica Concert'
      },
      category: {
        id: 4,
        name: 'Regular Seat'
      },
      totalAvailability: 1000,
      available: 700
    },
    {
      id: 5,
      name: 'Swan Lake - Regular Seat',
      price: 100,
      event: {
        id: 2,
        name: 'Broadway Play'
      },
      category: {
        id: 4,
        name: 'Regular Seat'
      },
      totalAvailability: 1200,
      available: 940
    },
    {
      id: 6,
      name: 'Swan Lake - Premium Seat',
      price: 150,
      event: {
        id: 2,
        name: 'Broadway Play'
      },
      category: {
        id: 5,
        name: 'Premium Seat'
      },
      totalAvailability: 100,
      available: 30
    },
    {
      id: 7,
      name: 'Swan Lake - VIP Pass',
      price: 200,
      event: {
        id: 2,
        name: 'Broadway Play'
      },
      category: {
        id: 2,
        name: 'VIP Pass'
      },
      totalAvailability: 80,
      available: 15
    },
    {
      id: 8,
      name: 'Swan Lake - General Admission',
      price: 80,
      event: {
        id: 2,
        name: 'Broadway Play'
      },
      category: {
        id: 1,
        name: 'General Admission'
      },
      totalAvailability: 700,
      available: 420
    },
    {
      id: 9,
      name: 'EURO 2024 Finals - General Admission',
      price: 80,
      event: {
        id: 3,
        name: 'Football Match'
      },
      category: {
        id: 1,
        name: 'General Admission'
      },
      totalAvailability: 5000,
      available: 2370
    },
    {
      id: 10,
      name: 'EURO 2024 Finals - VIP Pass',
      price: 200,
      event: {
        id: 3,
        name: 'Football Match'
      },
      category: {
        id: 2,
        name: 'VIP Pass'
      },
      totalAvailability: 500,
      available: 310
    },
    {
      id: 11,
      name: 'EURO 2024 Finals - Front Row Seat',
      price: 200,
      event: {
        id: 3,
        name: 'Football Match'
      },
      category: {
        id: 3,
        name: 'Front Row Seat'
      },
      totalAvailability: 100,
      available: 45
    }
  ]

  const ticketCategories = [
    {
      id: 1,
      name: 'General Admission',
      description: 'General Admission ticket'
    },
    {
      id: 2,
      name: 'VIP Pass',
      description: 'Access to VIP area with complimentary drinks'
    },
    {
      id: 3,
      name: 'Front Row Seat',
      description: 'Best seats in the house'
    },
    {
      id: 4,
      name: 'Regular Seat',
      description: 'Standard seating'
    },
    {
      id: 5,
      name: 'Premium Seat',
      description: 'Premium seating with better view'
    }
  ]

  const events = [
    {
      id: 1,
      name: 'Metallica Concert',
      date: '2024-07-01T20:00:00',
      location: 'Camp nou, Barcelona, Spain',
      description: 'A live rock concert featuring top bands.'
    },
    {
      id: 2,
      name: 'Broadway Play',
      date: '2024-07-10T18:00:00',
      location: 'Adelphi Theatre, London, UK',
      description: 'A classic Broadway play.'
    },
    {
      id: 3,
      name: 'Football Match',
      date: '2024-08-15T16:00:00',
      location: 'Allianz Arena, Munich, Germany',
      description: 'Championship football match.'
    },
    {
      id: 4,
      name: 'Jazz Festival',
      date: '2024-07-20T19:00:00',
      location: 'Montreux, Switzerland',
      description: 'An international jazz festival.'
    },
    {
      id: 5,
      name: 'Tech Conference',
      date: '2024-09-05T09:00:00',
      location: 'Silicon Valley, USA',
      description: 'A conference showcasing the latest in tech.'
    },
    {
      id: 6,
      name: 'Art Exhibition',
      date: '2024-06-25T10:00:00',
      location: 'Louvre Museum, Paris, France',
      description: 'An exhibition of modern art.'
    },
    {
      id: 7,
      name: 'Food Festival',
      date: '2024-07-15T12:00:00',
      location: 'Sydney, Australia',
      description: 'A festival celebrating diverse cuisines.'
    },
    {
      id: 8,
      name: 'Book Fair',
      date: '2024-10-10T10:00:00',
      location: 'Frankfurt, Germany',
      description: 'An international book fair.'
    },
    {
      id: 9,
      name: 'Marathon',
      date: '2024-08-20T07:00:00',
      location: 'New York City, USA',
      description: 'A city-wide marathon event.'
    },
    {
      id: 10,
      name: 'Film Premiere',
      date: '2024-09-25T18:00:00',
      location: 'Los Angeles, USA',
      description: 'Premiere of a highly anticipated film.'
    },
    {
      id: 11,
      name: 'Opera Performance',
      date: '2024-07-30T20:00:00',
      location: 'La Scala, Milan, Italy',
      description: 'A performance of a classic opera.'
    },
    {
      id: 12,
      name: 'Science Fair',
      date: '2024-11-05T09:00:00',
      location: 'Boston, USA',
      description: 'A fair displaying scientific projects.'
    },
    {
      id: 13,
      name: 'Winter Carnival',
      date: '2024-12-15T17:00:00',
      location: 'Quebec City, Canada',
      description: 'A carnival with winter activities and events.'
    }
  ]

  const server = createServer({
    environment,

    models: {
      user: Model,
      authUser: Model,
      category: Model,
      event: Model,
      ticket: Model
    },

    seeds(server) {
      events.forEach((event) => {
        server.create('event', event)
      })

      ticketCategories.forEach((category) => {
        server.create('category', category)
      })

      tickets.forEach((ticket) => {
        server.create('ticket', ticket)
      })

      server.create('authUser', {
        id: '001122',
        name: 'Vlad',
        lastName: 'Stankovic',
        email: 'test@test.com',
        password: 'test'
      })
    },

    routes() {
      this.urlPrefix = 'http://localhost:8081'
      this.namespace = 'api'

      this.get(
        '/users',
        (schema) => {
          return schema.users.all()
        },
        { timing: 1000 }
      )

      this.post(
        '/auth/login',
        (schema, request) => {
          const attrs = JSON.parse(request.requestBody)
          const authUser = schema.authUsers.findBy({ email: attrs.email })

          if (!authUser || authUser.password !== attrs.password) {
            return new Response(401, {}, { error: 'Invalid email or password' })
          }

          return new Response(
            200,
            {},
            { id: authUser.id, name: authUser.name, email: authUser.email }
          )
        },
        { timing: 1000 }
      )

      // CATEGORIES ///////////////////////////////////////////

      this.get(
        '/categories',
        (schema) => {
          return schema.categories.all()
        },
        { timing: 1000 }
      )

      this.get(
        '/categories/:id',
        (schema, request) => {
          const id = request.params.id
          const category = schema.categories.find(id)
          if (category) {
            return category
          } else {
            return new Response(404, {
              body: JSON.stringify({ message: 'Category not found' })
            })
          }
        },
        { timing: 1000 }
      )

      this.patch(
        '/categories/:id',
        (schema, request) => {
          const id = request.params.id
          const updatedEventData = JSON.parse(request.requestBody)
          const event = schema.categories.find(id)

          if (event) {
            event.update(updatedEventData)
            return event
          } else {
            return new Response(404, {
              body: JSON.stringify({ message: 'Event not found' })
            })
          }
        },
        { timing: 1000 }
      )

      this.post(
        '/categories',
        (schema, request) => {
          const attrs = JSON.parse(request.requestBody)
          return schema.categories.create(attrs)
        },
        { timing: 1000 }
      )

      this.delete('/categories/:id', (schema, request) => {
        const id = request.params.id
        const event = schema.categories.find(id)

        if (event) {
          // Delete the event
          event.destroy()
          return new Response(204, { body: '' })
        } else {
          return new Response(404, {
            body: JSON.stringify({ message: 'Event not found' })
          })
        }
      })

      // EVENTS ///////////////////////////////////////////

      this.get(
        '/events',
        (schema) => {
          return schema.events.all()
        },
        { timing: 1000 }
      )

      this.patch(
        '/events/:id',
        (schema, request) => {
          const id = request.params.id
          const updatedEventData = JSON.parse(request.requestBody)
          const event = schema.events.find(id)

          if (event) {
            event.update(updatedEventData)
            return event
          } else {
            return new Response(404, {
              body: JSON.stringify({ message: 'Event not found' })
            })
          }
        },
        { timing: 1000 }
      )

      this.post(
        '/events',
        (schema, request) => {
          const attrs = JSON.parse(request.requestBody)
          return schema.events.create(attrs)
        },
        { timing: 1000 }
      )

      this.delete('/events/:id', (schema, request) => {
        const id = request.params.id
        const event = schema.events.find(id)

        if (event) {
          // Delete the event
          event.destroy()
          return new Response(204, { body: '' })
        } else {
          return new Response(404, {
            body: JSON.stringify({ message: 'Event not found' })
          })
        }
      })

      // TICKETS ///////////////////////////////////////////

      this.get(
        '/tickets',
        (schema) => {
          return schema.tickets.all()
        },
        { timing: 1000 }
      )

      this.get(
        '/tickets/:id',
        (schema, request) => {
          const id = request.params.id
          const category = schema.tickets.find(id)
          if (category) {
            return category
          } else {
            return new Response(404, {
              body: JSON.stringify({ message: 'Category not found' })
            })
          }
        },
        { timing: 1000 }
      )

      this.patch(
        '/tickets/:id',
        (schema, request) => {
          const id = request.params.id
          const updatedEventData = JSON.parse(request.requestBody)
          const event = schema.tickets.find(id)

          if (event) {
            event.update(updatedEventData)
            return event
          } else {
            return new Response(404, {
              body: JSON.stringify({ message: 'Event not found' })
            })
          }
        },
        { timing: 1000 }
      )

      this.post(
        '/tickets',
        (schema, request) => {
          const attrs = JSON.parse(request.requestBody)
          return schema.tickets.create(attrs)
        },
        { timing: 1000 }
      )

      this.delete('/tickets/:id', (schema, request) => {
        const id = request.params.id
        const event = schema.tickets.find(id)

        if (event) {
          // Delete the event
          event.destroy()
          return new Response(204, { body: '' })
        } else {
          return new Response(404, {
            body: JSON.stringify({ message: 'Event not found' })
          })
        }
      })
    }
  })

  return server
}
