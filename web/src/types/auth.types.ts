export type AuthStateType = {
  userId: string | null
  firstName: string | null
  lastName: string | null
  email: string | null
  loading: boolean
  error: string | null
}

export type AuthResponseType = {
  userId: string | null
  firstName: string | null
  lastName: string | null
  email: string | null
  loading: boolean
  error: string | null
}

export type LoginPayloadType = {
  email: string
  password: string
}
