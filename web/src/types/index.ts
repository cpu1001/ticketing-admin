import type { AuthStateType } from './auth.types'
import type { TicketsStateType } from './tickets.types'
import type { EventsStateType } from './events.types'
import type { CategoriesStateType } from './categories.types'

export type RootStateType = {
  auth: AuthStateType
  tickets: TicketsStateType
  events: EventsStateType
  categories: CategoriesStateType
}
