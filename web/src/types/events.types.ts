export type EventType = {
  id?: string
  name: string
  date: string
  location: string
  description: string
}

export type EventsStateType = {
  data: EventType[] | null
  loading: boolean
  error: string | null
  currentEvent: {
    event: EventType | null
    loading: boolean
    error: string | null
  } | null
}
