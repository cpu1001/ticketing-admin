export type CategoryType = {
  id?: string
  name: string
  description: string
}

export type CategoriesStateType = {
  data: CategoryType[] | null
  loading: boolean
  error: string | null
  currentCategory: {
    category: CategoryType | null
    loading: boolean
    error: string | null
  } | null
}
