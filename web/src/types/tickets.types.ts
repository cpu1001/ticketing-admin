export type TicketType = {
  id?: string
  name: string
  price: number
  event: {
    id: string
    name: string
  }
  category: {
    id: string
    name: string
  }
  totalAvailability: number
  available: number
}

export type TicketsStateType = {
  data: TicketType[] | null
  loading: boolean
  error: string | null
  currentTicket: {
    ticket: TicketType | null
    loading: boolean
    error: string | null
  } | null
}
