export default [
  {
    path: '/',
    name: 'Home',
    component: () => import('../views/tickets/TicketsListingView.vue'),
    meta: { requiresAuth: true }
  },
  {
    path: '/tickets',
    name: 'TicketsListing',
    component: () => import('../views/tickets/TicketsListingView.vue'),
    meta: { requiresAuth: true }
  },
  {
    path: '/categories',
    name: 'CategoriesListing',
    component: () => import('../views/categories/CategoriesListingView.vue'),
    meta: { requiresAuth: true }
  },
  {
    path: '/events',
    name: 'EventsListing',
    component: () => import('../views/events/EventsListingView.vue'),
    meta: { requiresAuth: true }
  }
]
