import { createRouter, createWebHistory } from 'vue-router'

import PublicRoutes from './public-routes'
import ProtectedRoutes from './protected-routes'

import { isAuthenticated } from '@/utils'

const router = createRouter({
  history: createWebHistory(import.meta.env.BASE_URL),
  routes: [...PublicRoutes, ...ProtectedRoutes]
})

router.beforeEach((to, from, next) => {
  if (to.matched.some((route) => route.meta.requiresAuth)) {
    if (!isAuthenticated()) {
      next({ name: 'Login' })
    } else {
      next()
    }
  } else {
    if (to.name === 'Login' && isAuthenticated()) {
      next({ name: 'Home' })
      return
    }
    next()
  }
})

export default router
