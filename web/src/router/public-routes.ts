export default [
  {
    path: '/login',
    name: 'Login',
    component: () => import('../views/auth/LoginView.vue')
  },
  {
    path: '/logout',
    name: 'Logout',
    component: () => import('../views/auth/LogoutView.vue')
  }
]
