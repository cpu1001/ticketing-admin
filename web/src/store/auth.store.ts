import type { AuthStateType, LoginPayloadType } from '../types/auth.types'
import AuthService from '../services/auth.service'

export const authStore = {
  state(): AuthStateType {
    return {
      userId: null,
      firstName: null,
      lastName: null,
      email: null,
      loading: false,
      error: null
    }
  },
  actions: {
    async loginUser({ commit }, payload: LoginPayloadType) {
      commit('login')
      try {
        const response = await AuthService.login(payload)
        commit('loginSuccess', response.data)
        return true
      } catch (error) {
        console.log('error', error)
        commit('loginFailed', error.response.data.error)
        return false
      }
    }
  },
  mutations: {
    login(state: AuthStateType) {
      state.loading = true
      state.error = null
    },
    loginSuccess(state: AuthStateType, payload: LoginPayloadType) {
      state.email = payload.email
      state.loading = false
      localStorage.setItem('email', payload.email)
    },
    loginFailed(state: AuthStateType, payload: string) {
      state.error = payload
      state.loading = false
      localStorage.removeItem('email')
    },
    logout(state: AuthStateType) {
      state.email = null
      state.error = null
      localStorage.removeItem('email')
    },
    initializeStore(state: AuthStateType) {
      state.email = localStorage.getItem('email')
      if (localStorage.getItem('email')) {
        state.email = localStorage.getItem('email')
      }
    }
  },
  getters: {
    isLoggedIn(state: AuthStateType) {
      return state.email !== null
    }
  }
}
