import type { TicketsStateType, TicketType } from '@/types/tickets.types'

import TicketsService from '../services/tickets.service'

export const ticketsStore = {
  state(): TicketsStateType {
    return {
      data: null,
      loading: false,
      error: null,
      currentTicket: null
    }
  },
  actions: {
    async getTickets({ commit }) {
      commit('getTickets')
      try {
        const response = await TicketsService.getTickets()
        commit('getTicketsSuccess', response.data.tickets)
      } catch (error) {
        commit('getTicketsFailed', error.response.data.error)
      }
    },

    async getTicket({ commit }, id: string) {
      commit('getTicket', id)
      try {
        const response = await TicketsService.getTicket(id)
        commit('getTicketSuccess', response.data)
      } catch (error) {
        commit('getTicketFailed', error.response ? error.response.data.error : 'An error occurred')
      }
    },

    async updateTicket({ commit }, data: TicketType) {
      commit('updateTicket')
      try {
        const response = await TicketsService.updateTicket(data)
        commit('updateTicketSuccess', response.data.ticket)
      } catch (error) {
        commit(
          'updateTicketFailed',
          error.response ? error.response.data.error : 'An error occurred'
        )
      }
    },

    async createTicket({ commit }, data: TicketType) {
      commit('createTicket')
      try {
        const response = await TicketsService.createTicket(data)
        commit('createTicketSuccess', response.data.ticket)
      } catch (error) {
        commit(
          'createTicketFailed',
          error.response ? error.response.data.error : 'An error occurred'
        )
      }
    },

    async deleteTicket({ commit }, data: TicketType) {
      commit('deleteTicket')
      try {
        await TicketsService.deleteTicket(data)
        commit('deleteTicketSuccess', data)
      } catch (error) {
        commit(
          'deleteTicketFailed',
          error.response ? error.response.data.error : 'An error occurred'
        )
      }
    }
  },
  mutations: {
    getTickets(state: TicketsStateType) {
      state.loading = true
      state.error = null
    },
    getTicketsSuccess(state: TicketsStateType, payload: TicketType[]) {
      state.data = payload
      state.loading = false
    },
    getTicketsFailed(state: TicketsStateType, payload: string) {
      state.error = payload
      state.loading = false
    },
    getTicket(state: TicketsStateType) {
      state.currentTicket = {
        ticket: null,
        loading: true,
        error: null
      }
    },
    getTicketSuccess(state: TicketsStateType, payload: TicketType) {
      state.currentTicket = {
        ticket: payload,
        loading: false,
        error: null
      }
    },
    getTicketFailed(state: TicketsStateType, payload: string) {
      state.currentTicket = {
        ...state.currentTicket,
        loading: false,
        error: payload
      }
    },
    updateTicket(state: TicketsStateType) {
      state.currentTicket = {
        ...state.currentTicket,
        loading: true,
        error: null
      }
    },
    updateTicketSuccess(state: TicketsStateType, payload: TicketType) {
      state.data = state.data.map((ticket: TicketType) => {
        if (ticket.id === payload.id) {
          return payload
        }
        return ticket
      })

      state.currentTicket = {
        ticket: payload,
        loading: false,
        error: null
      }
    },
    updateTicketFailed(state: TicketsStateType, payload: string) {
      state.currentTicket = {
        ...state.currentTicket,
        loading: false,
        error: payload
      }
    },
    createTicket(state: TicketsStateType) {
      state.currentTicket = {
        ...state.currentTicket,
        loading: true,
        error: null
      }
    },
    createTicketSuccess(state: TicketsStateType, payload: TicketType) {
      state.data = [...state.data, payload]

      state.currentTicket = {
        ticket: payload,
        loading: false,
        error: null
      }
    },
    createTicketFailed(state: TicketsStateType, payload: string) {
      state.currentTicket = {
        ...state.currentTicket,
        loading: false,
        error: payload
      }
    },
    deleteTicket(state: TicketsStateType) {
      state.currentTicket = {
        ...state.currentTicket,
        loading: true,
        error: null
      }
    },
    deleteTicketSuccess(state: TicketsStateType, payload: TicketType) {
      state.data = state.data.filter((ticket: TicketType) => {
        if (ticket.id !== payload.id) {
          return ticket
        }
      })

      state.currentTicket = {
        ticket: null,
        loading: false,
        error: null
      }
    },
    deleteTicketFailed(state: TicketsStateType, payload: string) {
      state.currentTicket = {
        ...state.currentTicket,
        loading: false,
        error: payload
      }
    }
  }
}
