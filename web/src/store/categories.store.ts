import type { CategoriesStateType, CategoryType } from '@/types/categories.types'

import CategoriesService from '@/services/categories.service'

export const categoriesStore = {
  state(): CategoriesStateType {
    return {
      data: null,
      loading: false,
      error: null,
      currentCategory: null
    }
  },
  actions: {
    async getCategories({ commit }) {
      commit('getCategories')
      try {
        const response = await CategoriesService.getCategories()
        commit('getCategoriesSuccess', response.data.categories)
      } catch (error) {
        commit('getCategoriesFailed', error.response.data.error)
      }
    },

    async getCategory({ commit }, id: string) {
      commit('getCategory', id)
      try {
        const response = await CategoriesService.getCategory(id)
        commit('getCategorySuccess', response.data)
      } catch (error) {
        commit(
          'getCategoryFailed',
          error.response ? error.response.data.error : 'An error occurred'
        )
      }
    },

    async updateCategory({ commit }, categoryData: CategoryType) {
      commit('updateCategory')
      try {
        const response = await CategoriesService.updateCategory(categoryData)
        commit('updateCategorySuccess', response.data.category)
      } catch (error) {
        commit(
          'updateCategoryFailed',
          error.response ? error.response.data.error : 'An error occurred'
        )
      }
    },

    async createCategory({ commit }, data: CategoryType) {
      commit('createCategory')
      try {
        const response = await CategoriesService.createCategory(data)
        commit('createCategorySuccess', response.data.category)
      } catch (error) {
        commit(
          'createCategoryFailed',
          error.response ? error.response.data.error : 'An error occurred'
        )
      }
    },

    async deleteCategory({ commit }, data: CategoryType) {
      commit('deleteCategory')
      try {
        await CategoriesService.deleteCategory(data)
        commit('deleteCategorySuccess', data)
      } catch (error) {
        commit(
          'deleteCategoryFailed',
          error.response ? error.response.data.error : 'An error occurred'
        )
      }
    }
  },
  mutations: {
    getCategories(state: CategoriesStateType) {
      state.loading = true
      state.error = null
    },
    getCategoriesSuccess(state: CategoriesStateType, payload: CategoryType[]) {
      state.data = payload
      state.loading = false
    },
    getCategoriesFailed(state: CategoriesStateType, payload: string) {
      state.error = payload
      state.loading = false
    },
    getCategory(state: CategoriesStateType) {
      state.currentCategory = {
        category: null,
        loading: true,
        error: null
      }
    },
    getCategorySuccess(state: CategoriesStateType, payload: CategoryType) {
      state.currentCategory = {
        category: payload,
        loading: false,
        error: null
      }
    },
    getCategoryFailed(state: CategoriesStateType, payload: string) {
      state.currentCategory = {
        ...state.currentCategory,
        loading: false,
        error: payload
      }
    },
    updateCategory(state: CategoriesStateType) {
      state.currentCategory = {
        ...state.currentCategory,
        loading: true,
        error: null
      }
    },
    updateCategorySuccess(state: CategoriesStateType, payload: CategoryType) {
      state.data = state.data.map((category) => {
        if (category.id === payload.id) {
          return payload
        }
        return category
      })

      state.currentCategory = {
        category: payload,
        loading: false,
        error: null
      }
    },
    updateCategoryFailed(state: CategoriesStateType, payload: string) {
      state.currentCategory = {
        ...state.currentCategory,
        loading: false,
        error: payload
      }
    },
    createCategory(state: CategoriesStateType) {
      state.currentCategory = {
        ...state.currentCategory,
        loading: true,
        error: null
      }
    },
    createCategorySuccess(state: CategoriesStateType, payload: CategoryType) {
      state.data = [...state.data, payload]

      state.currentCategory = {
        category: payload,
        loading: false,
        error: null
      }
    },
    createCategoryFailed(state: CategoriesStateType, payload: string) {
      state.currentCategory = {
        ...state.currentCategory,
        loading: false,
        error: payload
      }
    },
    deleteCategory(state: CategoriesStateType) {
      state.currentCategory = {
        ...state.currentCategory,
        loading: true,
        error: null
      }
    },
    deleteCategorySuccess(state: CategoriesStateType, payload: CategoryType) {
      state.data = state.data.filter((category) => {
        if (category.id !== payload.id) {
          return category
        }
      })

      state.currentCategory = {
        category: null,
        loading: false,
        error: null
      }
    },
    deleteCategoryFailed(state: CategoriesStateType, payload: string) {
      state.currentCategory = {
        ...state.currentCategory,
        loading: false,
        error: payload
      }
    }
  }
}
