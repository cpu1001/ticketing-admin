import { createStore } from 'vuex'

import { authStore } from './auth.store'
import { ticketsStore } from './tickets.store'
import { eventsStore } from './events.store'
import { categoriesStore } from './categories.store'

export const store = createStore({
  modules: {
    auth: authStore,
    tickets: ticketsStore,
    events: eventsStore,
    categories: categoriesStore
  }
})
