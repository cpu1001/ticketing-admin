import type { EventsStateType, EventType } from '@/types/events.types'

import EventsService from '@/services/events.service'

export const eventsStore = {
  state(): EventsStateType {
    return {
      data: null,
      loading: false,
      error: null,
      currentEvent: null
    }
  },
  actions: {
    async getEvents({ commit }) {
      commit('getEvents')
      try {
        const response = await EventsService.getEvents()
        commit('getEventsSuccess', response.data.events)
      } catch (error) {
        commit('getEventsFailed', error.response.data.error)
      }
    },

    async getEvent({ commit }, eventId: string) {
      commit('getEvent')
      try {
        const response = await EventsService.getEvent(eventId)
        commit('getEventSuccess', response.data)
      } catch (error) {
        commit('getEventFailed', error.response ? error.response.data.error : 'An error occurred')
      }
    },

    async updateEvent({ commit }, eventData: EventType) {
      commit('updateEvent')
      try {
        const response = await EventsService.updateEvent(eventData)
        commit('updateEventSuccess', response.data.event)
      } catch (error) {
        commit(
          'updateEventFailed',
          error.response ? error.response.data.error : 'An error occurred'
        )
      }
    },

    async createEvent({ commit }, eventData: EventType) {
      commit('createEvent')
      try {
        const response = await EventsService.createEvent(eventData)
        commit('createEventSuccess', response.data.event)
      } catch (error) {
        commit(
          'createEventFailed',
          error.response ? error.response.data.error : 'An error occurred'
        )
      }
    },

    async deleteEvent({ commit }, eventData: EventType) {
      commit('deleteEvent')
      try {
        await EventsService.deleteEvent(eventData)
        commit('deleteEventSuccess', eventData)
      } catch (error) {
        commit(
          'deleteEventFailed',
          error.response ? error.response.data.error : 'An error occurred'
        )
      }
    }
  },
  mutations: {
    getEvents(state: EventsStateType) {
      state.loading = true
      state.error = null
    },
    getEventsSuccess(state: EventsStateType, payload: EventType[]) {
      state.data = payload
      state.loading = false
    },
    getEventsFailed(state: EventsStateType, payload: string) {
      state.error = payload
      state.loading = false
    },
    getEvent(state: EventsStateType) {
      state.currentEvent = {
        event: null,
        loading: true,
        error: null
      }
    },
    getEventSuccess(state: EventsStateType, payload: EventType) {
      state.currentEvent = {
        event: payload,
        loading: false,
        error: null
      }
    },
    getEventFailed(state: EventsStateType, payload: string) {
      state.currentEvent = {
        ...state.currentEvent,
        loading: false,
        error: payload
      }
    },
    updateEvent(state: EventsStateType) {
      state.currentEvent = {
        ...state.currentEvent,
        loading: true,
        error: null
      }
    },
    updateEventSuccess(state: EventsStateType, payload: EventType) {
      state.data = state.data.map((event: EventType) => {
        if (event.id === payload.id) {
          return payload
        }
        return event
      })

      state.currentEvent = {
        event: payload,
        loading: false,
        error: null
      }
    },
    updateEventFailed(state: EventsStateType, payload: string) {
      state.currentEvent = {
        ...state.currentEvent,
        loading: false,
        error: payload
      }
    },
    createEvent(state: EventsStateType) {
      state.currentEvent = {
        ...state.currentEvent,
        loading: true,
        error: null
      }
    },
    createEventSuccess(state: EventsStateType, payload: EventType) {
      state.data = [...state.data, payload]

      state.currentEvent = {
        event: payload,
        loading: false,
        error: null
      }
    },
    createEventFailed(state: EventsStateType, payload: string) {
      state.currentEvent = {
        ...state.currentEvent,
        loading: false,
        error: payload
      }
    },
    deleteEvent(state: EventsStateType) {
      state.currentEvent = {
        ...state.currentEvent,
        loading: true,
        error: null
      }
    },
    deleteEventSuccess(state: EventsStateType, payload: EventType) {
      state.data = state.data.filter((event: EventType) => {
        if (event.id !== payload.id) {
          return event
        }
      })

      state.currentEvent = {
        event: null,
        loading: false,
        error: null
      }
    },
    deleteEventFailed(state: EventsStateType, payload: string) {
      state.currentEvent = {
        ...state.currentEvent,
        loading: false,
        error: payload
      }
    }
  }
}
