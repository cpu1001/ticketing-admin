import { describe, it, expect, vi, beforeEach } from 'vitest';
import { ticketsStore } from '../tickets.store';
import TicketsService from '../../services/tickets.service';
import type { TicketsStateType, TicketType } from '../../types/tickets.types';

vi.mock('../../services/tickets.service', () => ({
  default: {
    getTickets: vi.fn(),
    getTicket: vi.fn(),
    updateTicket: vi.fn(),
    createTicket: vi.fn(),
    deleteTicket: vi.fn(),
  },
}));

describe('ticketsStore', () => {
  let state: TicketsStateType;
  let commit: any;

  beforeEach(() => {
    state = ticketsStore.state();
    commit = vi.fn();
  });

  describe('actions', () => {
    it('should call getTickets and commit getTicketsSuccess on success', async () => {
      const tickets = [{ id: '1', title: 'Ticket 1' }];
      (TicketsService.getTickets as any).mockResolvedValue({ data: { tickets } });

      await ticketsStore.actions.getTickets({ commit });

      expect(TicketsService.getTickets).toHaveBeenCalled();
      expect(commit).toHaveBeenCalledWith('getTickets');
      expect(commit).toHaveBeenCalledWith('getTicketsSuccess', tickets);
    });

    it('should call getTickets and commit getTicketsFailed on error', async () => {
      const error = { response: { data: { error: 'Error' } } };
      (TicketsService.getTickets as any).mockRejectedValue(error);

      await ticketsStore.actions.getTickets({ commit });

      expect(TicketsService.getTickets).toHaveBeenCalled();
      expect(commit).toHaveBeenCalledWith('getTickets');
      expect(commit).toHaveBeenCalledWith('getTicketsFailed', 'Error');
    });

    it('should call getTicket and commit getTicketSuccess on success', async () => {
      const ticket = { id: '1', title: 'Ticket 1' };
      (TicketsService.getTicket as any).mockResolvedValue({ data: ticket });

      await ticketsStore.actions.getTicket({ commit }, '1');

      expect(TicketsService.getTicket).toHaveBeenCalledWith('1');
      expect(commit).toHaveBeenCalledWith('getTicket', '1');
      expect(commit).toHaveBeenCalledWith('getTicketSuccess', ticket);
    });

    it('should call getTicket and commit getTicketFailed on error', async () => {
      const error = { response: { data: { error: 'Error' } } };
      (TicketsService.getTicket as any).mockRejectedValue(error);

      await ticketsStore.actions.getTicket({ commit }, '1');

      expect(TicketsService.getTicket).toHaveBeenCalledWith('1');
      expect(commit).toHaveBeenCalledWith('getTicket', '1');
      expect(commit).toHaveBeenCalledWith('getTicketFailed', 'Error');
    });

    it('should call updateTicket and commit updateTicketSuccess on success', async () => {
      const ticket = { id: '1', title: 'Updated Ticket' };
      (TicketsService.updateTicket as any).mockResolvedValue({ data: { ticket } });

      await ticketsStore.actions.updateTicket({ commit }, ticket);

      expect(TicketsService.updateTicket).toHaveBeenCalledWith(ticket);
      expect(commit).toHaveBeenCalledWith('updateTicket');
      expect(commit).toHaveBeenCalledWith('updateTicketSuccess', ticket);
    });

    it('should call updateTicket and commit updateTicketFailed on error', async () => {
      const error = { response: { data: { error: 'Error' } } };
      (TicketsService.updateTicket as any).mockRejectedValue(error);

      await ticketsStore.actions.updateTicket({ commit }, { id: '1', title: 'Updated Ticket' });

      expect(TicketsService.updateTicket).toHaveBeenCalledWith({ id: '1', title: 'Updated Ticket' });
      expect(commit).toHaveBeenCalledWith('updateTicket');
      expect(commit).toHaveBeenCalledWith('updateTicketFailed', 'Error');
    });

    it('should call createTicket and commit createTicketSuccess on success', async () => {
      const ticket = { id: '1', title: 'New Ticket' };
      (TicketsService.createTicket as any).mockResolvedValue({ data: { ticket } });

      await ticketsStore.actions.createTicket({ commit }, ticket);

      expect(TicketsService.createTicket).toHaveBeenCalledWith(ticket);
      expect(commit).toHaveBeenCalledWith('createTicket');
      expect(commit).toHaveBeenCalledWith('createTicketSuccess', ticket);
    });

    it('should call createTicket and commit createTicketFailed on error', async () => {
      const error = { response: { data: { error: 'Error' } } };
      (TicketsService.createTicket as any).mockRejectedValue(error);

      await ticketsStore.actions.createTicket({ commit }, { id: '1', title: 'New Ticket' });

      expect(TicketsService.createTicket).toHaveBeenCalledWith({ id: '1', title: 'New Ticket' });
      expect(commit).toHaveBeenCalledWith('createTicket');
      expect(commit).toHaveBeenCalledWith('createTicketFailed', 'Error');
    });

    it('deleteTicket should call deleteTicket and commit deleteTicketSuccess on success', async () => {
      const ticket = { id: '1', title: 'Delete Ticket' };
      (TicketsService.deleteTicket as any).mockResolvedValue({});

      await ticketsStore.actions.deleteTicket({ commit }, ticket);

      expect(TicketsService.deleteTicket).toHaveBeenCalledWith(ticket);
      expect(commit).toHaveBeenCalledWith('deleteTicket');
      expect(commit).toHaveBeenCalledWith('deleteTicketSuccess', ticket);
    });

    it('deleteTicket should call deleteTicket and commit deleteTicketFailed on error', async () => {
      const error = { response: { data: { error: 'Error' } } };
      (TicketsService.deleteTicket as any).mockRejectedValue(error);

      await ticketsStore.actions.deleteTicket({ commit }, { id: '1', title: 'Delete Ticket' });

      expect(TicketsService.deleteTicket).toHaveBeenCalledWith({ id: '1', title: 'Delete Ticket' });
      expect(commit).toHaveBeenCalledWith('deleteTicket');
      expect(commit).toHaveBeenCalledWith('deleteTicketFailed', 'Error');
    });
  });

  describe('mutations', () => {
    it('getTickets should set loading to true and error to null', () => {
      ticketsStore.mutations.getTickets(state);
      expect(state.loading).toBe(true);
      expect(state.error).toBe(null);
    });

    it('getTicketsSuccess should set data and loading to false', () => {
      const tickets = [{ id: '1', title: 'Ticket 1' }];
      ticketsStore.mutations.getTicketsSuccess(state, tickets);
      expect(state.data).toEqual(tickets);
      expect(state.loading).toBe(false);
    });

    it('getTicketsFailed should set error and loading to false', () => {
      ticketsStore.mutations.getTicketsFailed(state, 'Error');
      expect(state.error).toBe('Error');
      expect(state.loading).toBe(false);
    });

    // Add similar tests for the other mutations
  });
});
