import { describe, it, expect, vi, beforeEach } from 'vitest';
import { authStore } from '../auth.store';
import AuthService from '../..//services/auth.service';
import type { AuthStateType, LoginPayloadType } from '@/types/auth.types';

vi.mock('../..//services/auth.service', () => ({
  default: {
    login: vi.fn(),
  },
}));

describe('authStore', () => {
  let state: AuthStateType;
  let commit: any;

  beforeEach(() => {
    state = authStore.state();
    commit = vi.fn();
  });

  describe('actions', () => {
    it('loginUser should call AuthService.login and commit loginSuccess on success', async () => {
      const payload: LoginPayloadType = {
        email: 'vlad@example.com',
        password: 'password123',
      };
      const userData = {
        email: payload.email,
        userId: '123',
        firstName: 'Vlad',
        lastName: 'Stankovic',
      };

      (AuthService.login as unknown as jest.Mock).mockResolvedValue({ data: userData });

      await authStore.actions.loginUser({ commit }, payload);

      expect(AuthService.login).toHaveBeenCalledWith(payload);
      expect(commit).toHaveBeenCalledWith('login');
      expect(commit).toHaveBeenCalledWith('loginSuccess', userData);
    });

    it('loginUser should call AuthService.login and commit loginFailed on error', async () => {
      const payload: LoginPayloadType = {
        email: 'vlad@example.com',
        password: 'vladspassword',
      };
      const error = { response: { data: { error: 'Invalid credentials' } } };

      (AuthService.login as unknown as jest.Mock).mockRejectedValue(error);

      await authStore.actions.loginUser({ commit }, payload);

      expect(AuthService.login).toHaveBeenCalledWith(payload);
      expect(commit).toHaveBeenCalledWith('login');
      expect(commit).toHaveBeenCalledWith('loginFailed', 'Invalid credentials');
    });
  });

  describe('mutations', () => {
    it('should set loading to true and error to null', () => {
      authStore.mutations.login(state);
      expect(state.loading).toBe(true);
      expect(state.error).toBe(null);
    });

    it('should set email, loading to false, and update localStorage', () => {
      const payload: LoginPayloadType = {
        email: 'hello@vladislav.rs',
        password: 'helloitsme',
      };
      authStore.mutations.loginSuccess(state, payload);
      expect(state.email).toBe(payload.email);
      expect(state.loading).toBe(false);
      expect(localStorage.getItem('email')).toBe(payload.email);
    });

    it('should set error, loading to false, and remove email from localStorage', () => {
      const error = 'Invalid credentials';
      authStore.mutations.loginFailed(state, error);
      expect(state.error).toBe(error);
      expect(state.loading).toBe(false);
      expect(localStorage.getItem('email')).toBe(null);
    });

    it('should clear email and error, and remove email from localStorage', () => {
      state.email = 'vlad@test.com';
      state.error = 'Some error';
      authStore.mutations.logout(state);
      expect(state.email).toBe(null);
      expect(state.error).toBe(null);
      expect(localStorage.getItem('email')).toBe(null);
    });

    it('should initialize state.email from localStorage if present', () => {
      const storedEmail = 'vlad@test.com';
      localStorage.setItem('email', storedEmail);
      authStore.mutations.initializeStore(state);
      expect(state.email).toBe(storedEmail);
    });

    it('should not initialize state.email if localStorage does not have email', () => {
      localStorage.removeItem('email');
      authStore.mutations.initializeStore(state);
      expect(state.email).toBe(null);
    });
  });

  describe('getters', () => {
    it('isLoggedIn should return true if state.email is not null', () => {
      state.email = 'test@example.com';
      const isLoggedIn = authStore.getters.isLoggedIn(state);
      expect(isLoggedIn).toBe(true);
    });

    it('isLoggedIn should return false if state.email is null', () => {
      state.email = null;
      const isLoggedIn = authStore.getters.isLoggedIn(state);
      expect(isLoggedIn).toBe(false);
    });
  });
});